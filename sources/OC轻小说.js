/**
 * 搜索
 * @params {string} key
 * @returns {[{name, author, cover, detail}]}
 */
const search = (key) => {
  let response = GET(`http://www.ocacg.com/search.html?keyword=${encodeURI(key)}`)
  let array = []
  let $ = HTML.parse(response)
  $('div.result > ul > li').forEach((child) => {
    let $ = HTML.parse(child)
    array.push({
      name: $('h3').text(),
      author: $('p:nth-child(2) > span:nth-child(1) > a').text(),
      cover: $('img').attr('data-src'),
      detail: `http://www.ocacg.com${$('h3 > a').attr('href')}`,
    })
  })
  return JSON.stringify(array)
}

/**
 * 详情
 * @params {string} url
 * @returns {[{summary, status, category, words, update, lastChapter, catalog}]}
 */
const detail = (url) => {
  let response = GET(url)
  let $ = HTML.parse(response)
  let book = {
    summary: $('#book_desc').text(),
    status: $('img.state').attr('alt'),
    category: $('p.c05_t').text().replace("作品标签： ",""),
    words: $('div.inf02 > p:nth-child(3)').text().replace("总字数：","").replace("+",""),
    update: $('span.time').text(),
    lastChapter: $('a.zj').text(),
    catalog: `http://www.ocacg.com${$('a.mulu').attr('href')}`
  }
  return JSON.stringify(book)
}

/**
 * 目录
 * @params {string} url
 * @returns {[{name, url, vip}]}
 */
const catalog = (url) => {
  let response = GET(url)
  let $ = HTML.parse(response)
  let array = []
  $('div.fL_con > ul > li').forEach((chapter) => {
    let $ = HTML.parse(chapter)
    array.push({
      name: $('a').text(),
      url: `http://www.ocacg.com${$('a').attr('href')}`
    })
  })
  return JSON.stringify(array)
}

/**
 * 章节
 * @params {string} url
 * @returns {string}
 */
const chapter = (url) => {
  let response = GET(url)
  let $ = HTML.parse(response)
  return $('div.read-content')
}

var bookSource = JSON.stringify({
  name: "OC轻小说",
  url: "ocacg.com",
  version: 100
})