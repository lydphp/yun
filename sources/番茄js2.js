const baseUrl = "https://api5-normal-lf.fqnovel.com"

//搜索
const search = (key) => {
  let response = GET(`https://fanqienovel.com/api/author/search/search_book/v1?filter=127,127,127&page_count=10&page_index=0&query_type=0&query_word=${encodeURI(key)}&msToken=MRTLwA1ZOXwZbT-8UZGY2xtki-LP15QmOkDfbdPx3jW3QT9oGK3cDP3eL_jyUciNbetjiBWKg1WAwoWIYwr6ir3jfpo-mraagftyuZ0jLxhtWqrZkshG&X-Bogus=DFSzswVLWWTANtlRS1ryUipydRPl&_signature=_02B4Z6wo00001eXRvxwAAIDC0HI7xcPjtM3l0buAABu-12GHbhhg6GQRxMoNLDshSpb7v1HT7F0XmhexF7NtwuyKx5gnXca3GF5u56LcLWn-0Gh-C1WRLycIKHWnQhjbtdApp-jXA9dSUOcec5`)
  let array = []
  let $ = JSON.parse(response)
  $.data.search_book_data_list.forEach((child) => {
    array.push({
      name: child.book_name,
      author: child.author,
      cover: `https://p3-tt.byteimg.com/img/${child.thumb_uri}~240x312.jpg`,
      detail: `${baseUrl}/reading/bookapi/directory/all_items/v/?need_version=true&book_id=${child.book_id}&iid=2665637677906061&aid=1967&app_name=novelapp&version_code=495`,
    })
  })
  return JSON.stringify(array)
}

//详情
const detail = (url) => {
  let response = GET(url)
  let data = JSON.parse(response);
  let $ = data.data.book_info;
  let book = {
    summary: `${$.abstract}`,
    status: `${$.update_status}`,
    category: `${$.category}`,
    words: `${$.word_number}`,
    update: `${$.last_chapter_update_time}`,
    lastChapter: `${$.last_chapter_title}`,
    catalog: `https://api5-normal-lf.fqnovel.com/reading/bookapi/directory/all_items/v/?need_version=true&book_id=${$.book_id}&iid=2665637677906061&aid=1967&app_name=novelapp&version_code=495`
  }
  return JSON.stringify(book)
}

//目录
const catalog = (url) => {
  let response = GET(url)
  let $ = JSON.parse(response)
  let vlist = []
    let array = []
    let vidlist = []
    let list = $.data.item_data_list
    $.data.item_data_list.forEach((booklet) => {
        if (vidlist.indexOf(booklet.volume_name) == -1) {
            vlist.push(booklet);
            vidlist.push(booklet.volume_name)
        }
    })
    vlist.forEach((booklet) => {
        let vid = booklet.volume_name
        array.push({
            name: '◆◇'+String(booklet.volume_name)+'◇◆'
        })
        list.forEach((chapter) => {
            if (vid == chapter.volume_name) {
                array.push({
                    name: chapter.title,
                    url: `https://novel.snssdk.com/api/novel/book/reader/full/v1/?group_id=`+chapter.item_id+`&item_id=`+chapter.item_id                 
                })
            }
        })
    })
  return JSON.stringify(array)
}

//章节
const chapter = (url) => {
    let $ = JSON.parse(GET(url))
  return $.data.content.replace(/<div.+<\/div>/,"")
}

//排行榜
const rank = (title, category, page) => {
  let response = GET(`https://fanqienovel.com/api/author/library/book_list/v0/?page_count=18&page_index=${page}&gender=${title}&category_id=-1&creation_status=-1&word_count=-1&sort=0`)
  let $ = JSON.parse(response)
  let books = []
  $.data.book_list.forEach((child) => {
    books.push({
      name: child.book_name,
      author: child.author,
      cover: `https://p3-tt.byteimg.com/img/${child.thumb_uri}~240x312.jpg`,
      detail: `${baseUrl}/page/${child.book_id}`,
    })
  })
  return JSON.stringify({
    end:  $.data === null,
    books: books
  })
}

const ranks = [
    {
        title: {
            key: '1',
            value: '男生'
        }
    },
    {
        title: {
            key: '0',
            value: '女生'
        }
    }
]

var bookSource = JSON.stringify({
  name: "番茄js2",
  url: "novel.snom",
  version: 103,
  ranks: ranks
})