const header = ["User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.82 Safari/537.36"]
//搜索
const search = (key) => {
    let response = POST(`https://www.ixs.la/search.php`,{data:`searchkey=${encodeURI(key)}`,headers:header})
    let array = []
    let $ = HTML.parse(response)
    $('.item').forEach((child) => {
        let $ = HTML.parse(child)
        array.push({
            name: $('dl > dt > a').text(),
            author: $('dl > dt > span').text(),
            cover:`https://www.ixs.la${$('div.image > a > img').attr('src')}`,
            detail: `https://www.ixs.la${$('dl > dt > a').attr('href')}`,
        })

    })
    return JSON.stringify(array)
}

//详情
const detail = (url) => {
    let response = GET(url, {headers:header})
    let $ = HTML.parse(response)
    let book = {
        summary: $('meta[property=og:description]').attr("content"),
        status: $('meta[property=og:novel:status]').attr("content"),
        category: $('meta[property=og:novel:category]').attr("content"),
        update: $('meta[property=og:novel:update_time]').attr("content"),
        lastChapter: $('meta[property=og:novel:latest_chapter_name]').attr("content"),
        catalog: url
    }
    return JSON.stringify(book)
}

//目录
const catalog = (url) => {
    let response = GET(url, {headers:header})
    let $ = HTML.parse(response)
    let array = []
    $('#list > dl > dt:gt(1),dl > dd:gt(13)').forEach((chapter) => {
    let $ = HTML.parse(chapter)
    if ($('dt').text().length != 0) {
      array.push({ name: $('dt').text().replace(/\《.*\》/,"") })
    } else {
      array.push({
        name: $('a').text(),
        url: `https://www.ixs.la${$('a').attr('href')}`,
        vip: false
      })
    }
  })
    return JSON.stringify(array)
}

//章节
const chapter = (url) => {
    let response = GET(url,{headers:header})
    let $ = HTML.parse(response)
    return $("#content")
}




var bookSource = JSON.stringify({
    name: "爱看阅读",
    url: "www.ixs.la",
    version: 100
})