require('crypto-js')

const time = Math.round(new Date()/1000)

const search = (key) => {
  let response = GET(`https://get.xiashuzhan.com/api/get?action=search&key=${encodeURI(key)}&page=1&type=all&tktime=`+time+`&token=${CryptoJS.MD5(CryptoJS.enc.Utf8.parse("action=search&key="+key+"&page=1&type=all&tktime="+time).toString(CryptoJS.enc.Base64)+"xiashu.token")}`)
  let array = []
  let $ = JSON.parse(response)
  $.searchlist.forEach((child) => {
    array.push({
      name: child.novel.name,
      author: child.author.name,
      cover: child.novel.cover,
      detail: `a?k=${child.novel.id}&t=${child.last.time}`
    })
  })
  return JSON.stringify(array)
}

//详情
const detail = (url) => {
  let k = `${url.query("k")}`
  let t = `${url.query("t")}`
  let response = GET(`https://get.xiashuzhan.com/api/get?action=cover&id=`+k+`&lasttime=`+t+`&tktime=`+time+`&token=${CryptoJS.MD5(CryptoJS.enc.Utf8.parse("action=cover&id="+k+"&lasttime="+t+"&tktime="+time).toString(CryptoJS.enc.Base64)+"xiashu.token")}`)
  let $ = JSON.parse(response).cover
  let book = {
    summary: $.novel.intro,
    category: $.category.name,
    update: timestampToTime($.last.time),
    lastChapter: $.last.name,
    catalog: `a?k=${$.novel.id}&t=${$.last.time}`
  }
  return JSON.stringify(book)
}

function timestampToTime(timestamp) {
  var date = new Date(timestamp * 1000);
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1):date.getMonth()+1) + '-';
  var D = (date.getDate()< 10 ? '0'+date.getDate():date.getDate())+ ' ';
  var h = (date.getHours() < 10 ? '0'+date.getHours():date.getHours())+ ':';
  var m = (date.getMinutes() < 10 ? '0'+date.getMinutes():date.getMinutes()) + ':';
  var s = date.getSeconds() < 10 ? '0'+date.getSeconds():date.getSeconds();
  return Y+M+D+h+m+s;
}

//目录
const catalog = (url) => {
  let k = `${url.query("k")}`
  let t = `${url.query("t")}`
  let response = GET(`https://get.xiashuzhan.com/api/get?action=chapterlist&id=`+k+`&lasttime=`+t+`&tktime=`+time+`&token=${CryptoJS.MD5(CryptoJS.enc.Utf8.parse("action=chapterlist&id="+k+"&lasttime="+t+"&tktime="+time).toString(CryptoJS.enc.Base64)+"xiashu.token")}`)
  let $ = JSON.parse(response)
  let array = []
  $.chapterlist.forEach(chapter => {
    array.push({
      name: chapter.name,
      url: `a?k=`+k+`&t=${chapter.oid}`
    })
  })
  return JSON.stringify(array)
}

//章节
const chapter = (url) => {
  let k = `${url.query("k")}`
  let t = `${url.query("t")}`
  let $ = JSON.parse(GET(`https://get.xiashuzhan.com/api/get?action=appchapter&id=`+k+`&cid=`+t+`&lasttime=`+time+`&tktime=`+time+`&token=${CryptoJS.MD5(CryptoJS.enc.Utf8.parse("action=appchapter&id="+k+"&cid="+t+"&lasttime="+time+"&tktime="+time).toString(CryptoJS.enc.Base64)+"xiashu.token")}`))
  return $.data.content
}

var bookSource = JSON.stringify({
  name: "下书文学",
  url: "get.xiashuzhan.com",
  version: 100
})
